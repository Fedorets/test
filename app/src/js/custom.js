jQuery(document).ready(function($){

    $('.preloader').fadeOut();

    $('.burger').click(function() {
        $(this).toggleClass('burger--active');
        $('.header__user-block--authorized').toggleClass('header__user-block--open');
    });

    $('.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        adaptiveHeight: true,
        arrows: true,
        nextArrow: '<i class="icon-right slick-next slick-arrow icon--hover"></i>',
        prevArrow: '<i class="icon-left slick-prev slick-arrow icon--hover"></i>',
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '25px',
                    slidesToShow: 1
                }
            }
        ]
    });
});