var gulp = require('gulp'),
    sass = require('gulp-sass'),
    minify = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    sourcemap = require('gulp-sourcemaps'),
    rename = require('gulp-rename'),
    watch = require('gulp-watch'),
    notify = require('gulp-notify'),
    concat = require('gulp-concat'),
    browserSync = require('browser-sync').create();


gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./../"
        }
    });
});

gulp.task('styles', function() {
    gulp.src('scss/main.scss')
        .pipe(sourcemap.init())
        .pipe(sass())
        .pipe(rename({suffix: '.min'}))
        .pipe(minify())
        .pipe(sourcemap.write())
        // .pipe(notify('Style task complete'))
        .pipe(browserSync.stream())
        .pipe(gulp.dest('../css'))
});

gulp.task('scripts', function() {
    gulp.src(['js/lib/*.js', 'js/*.js'])
        .pipe(sourcemap.init())
        .pipe(concat('main.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(sourcemap.write())
        // .pipe(notify('Script task complete'))
        .pipe(browserSync.stream())
        .pipe(gulp.dest('../js'))
});

gulp.task('default', ['browser-sync'], function () {
    gulp.watch('scss/**/*.scss', ['styles']);
    gulp.watch('js/**/*.js', ['scripts']);
    gulp.watch('../*.html').on('change', browserSync.reload);
});